const random = require('random');
const nodeRandomName = require('node-random-name');
const moviesNames = require('movies-names');
const mongoose = require('mongoose');
const Movie = mongoose.model('Movie');

function randomNamesArray (number) {
	const names = [];

	for (let i = 0; i < number; i++) {
		names.push(nodeRandomName({
			seed: random.int(0, 10000000),
		}));
	}

	return names;
}

module.exports = {
	make: function (number) {
		const movies = [];

		for (let i = 0; i < number; i++) {
			const movie = new Movie();
			const _movie = moviesNames.random();

			movie.title = _movie.title;
			movie.directors = randomNamesArray(random.int(1, 4));
			movie.actors = _movie.cast;
			movie.genres = _movie.genres;
			movie.year = _movie.year;

			movies.push(movie);
		}

		return movies;
	},

	insert: async function ({
		number = 1, onlyOnCreation = true,
	}) {
		const count = await Movie.countDocuments();

		if (onlyOnCreation && count > 0) {
			console.log('The documents has not been inserted because ' +
				'this collection has at least 1 document inserted.');
			console.log('Pass the second argument as "false" if ' +
				'you want to ignore this constraint.');

			return;
		}

		const movies = this.make(number);

		try {
			const docs = await Movie.create(movies);

			console.log(`${docs.length} documents has been inserted.`);
		} catch (e) {
			console.error(e);
			console.error('Erro ao inserir dados dummy no banco de dados.');
		}
	},
};
