const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate-v2');
const config = require('@config');


const movieSchema = new Schema({
	title: {
		type: String,
		required: true,
		trim: true,
	},
	directors: {
		type: [ String ],
		required: true,
		trim: true,
	},
	cast: {
		type: [ String ],
		required: true,
		trim: true,
	},
	genres: {
		type: [ String ],
		required: true,
		trim: true,
	},
	year: {
		type: Number,
		required: true,
		trim: true,
	},
	averageScore: {
		type: Number,
		default: 0,
	},
	votes: {
		type: [
			{
				_id: {
					type: mongoose.SchemaTypes.ObjectId,
					ref: 'User',
				},
				score: {
					type: Number,
				},
			},
		],
		default: [],
	},
}, {
	timestamps: false,
});


/*
 * Plugins
 */
movieSchema.plugin(mongoosePaginate);


/*
 * ----- Indexes -----
 */
movieSchema.index({ title: 1 });
movieSchema.index({ directors: 1 });
movieSchema.index({ cast: 1 });
movieSchema.index({ genres: 1 });

/*
 * ----- Query helpers -----
 */

/*
 * ----- Pre -----
 */


/*
 * ----- Post -----
 */


/*
 * ----- Static methods -----
 */
movieSchema.statics.getPage = async function (query, page) {
	const fields =
		'title directors cast genres year averageScore numberOfVotes';

	const options = {
		limit: config.db.pageSize,
		page: page,
		select: fields,
	};


	return await mongoose.model('Movie').paginate(query, options);
};

/*
 * ----- Instance methods -----
 */
movieSchema.methods.saveVote = async function (userId, score) {
	let movie = this;
	let oldScore = 0;
	const vote = {
		_id: userId,
		score: score,
	};

	const oldVote = await mongoose.model('Movie').findOne({
		_id: movie._id,
	}).
		select({
			votes: {
				$elemMatch: {
					_id: userId,
				},
			},
			_id: false,
		});

	if (oldVote) {
		oldScore = oldVote.votes[0].score;
	}

	console.log(oldScore);

	movie.votes = await movie.votes.pull(userId);
	movie.votes.push(vote);
	movie.averageScore =
		movie.averageScore + ((score - oldScore) / movie.votes.length);
	movie = await movie.save();

	return movie;
};

mongoose.model('Movie', movieSchema);
