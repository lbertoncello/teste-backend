const create = require('./create');
const detailById = require('./detailById');
const getAll = require('./getAll');

const routes = {
	create: '',
	getAll: '',
	detailById: '/:id/detail',
	readById: '/:id',
};

module.exports = {
	routes,
	create,
	detailById,
	getAll,
};
