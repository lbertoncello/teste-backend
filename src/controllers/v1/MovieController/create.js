const mongoose = require('mongoose');
const Movie = mongoose.model('Movie');

async function create (req, res, next) {
	try {
		let movie = new Movie(req.body);

		movie = await movie.save();

		if (movie) {
			const data = {
				title: movie.title,
				directors: movie.directors,
				cast: movie.cast,
				genres: movie.genres,
				year: movie.year,
			};

			return res.status(200).json({
				success: true,
				message: 'Filme inserido com sucesso.',
				data: data,
			});
		}

		return res.status(422).json({
			success: false,
			message: 'Não foi possível inserir o filme.',
		});
	} catch (e) {
		next(e);
	}
}

module.exports = create;
