const mongoose = require('mongoose');
const Movie = mongoose.model('Movie');

function buildDbQuery (params) {
	const {
		title,
		director,
		cast,
		genre,
	} = params;
	const query = {};


	if (title) {
		query.title = title;
	}

	if (director) {
		query.directors = director;
	}

	if (cast) {
		query.cast = cast;
	}

	if (genre) {
		query.genres = genre;
	}

	return query;
}

function getPageNumber (params) {
	let { page } = params;

	if (!page) {
		page = 1;
	}

	return page;
}

async function getAll (req, res, next) {
	try {
		const query = buildDbQuery(req.query);
		const page = getPageNumber(req.query);
		const movies = await Movie.getPage(query, page);

		return res.status(200).json(movies);
	} catch (e) {
		next(e);
	}
}

module.exports = getAll;
