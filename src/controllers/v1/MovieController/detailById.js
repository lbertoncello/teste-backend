const mongoose = require('mongoose');
const Movie = mongoose.model('Movie');

async function getDetail (req, res, next) {
	try {
		const _id = req.params.id;
		const movie = await Movie.findById(_id);

		return res.status(200).json({
			success: true,
			message: 'Dados do filme acessados com sucesso.',
			isAuthenticated: true,
			data: {
				_id: movie._id,
				title: movie.title,
				directors: movie.directors,
				cast: movie.cast,
				genres: movie.genres,
				year: movie.year,
				averageScore: movie.averageScore,
				numberOfVotes: movie.votes.length,
				votes: movie.votes,
			},
		});
	} catch (e) {
		next(e);
	}
}

module.exports = getDetail;
