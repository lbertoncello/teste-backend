const express = require('express');
const router = express.Router();

const auth = require('@middlewares/auth');
const movieController = require('@controllers/v1/MovieController');

router.post(
	movieController.routes.create,
	auth.user,
	movieController.create,
);
router.get(
	movieController.routes.detailById,
	auth.allRoles,
	movieController.detailById,
);
router.get(
	movieController.routes.readById,
	auth.allRoles,
	movieController.detailById,
);
router.get(
	movieController.routes.getAll,
	auth.allRoles,
	movieController.getAll,
);

module.exports = router;
