module.exports = {
	secret: process.env.SECRET,
	expireTime: parseInt(process.env.EXPIRE_TIME),
};
